package com.jiusong.doordash.viewmodel

import android.app.Application
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.jiusong.doordash.data.Repository
import com.jiusong.doordash.data.model.Store
import com.jiusong.doordash.data.network.Resource
import com.jiusong.doordash.location.LocationLiveData
import com.jiusong.doordash.location.LocationModel
import kotlinx.coroutines.launch

/**
 * Created by jiusong.gao on 1/29/21.
 */
class MapViewModel @ViewModelInject constructor(application: Application, private val repository: Repository) : AndroidViewModel(application) {

    val locationData = LocationLiveData(application)

    var stores = MutableLiveData<Resource<List<Store>>>()

    fun loadNearByStores(locationModel: LocationModel) {
        viewModelScope.launch {
            val list = repository.getNearByStores(locationModel.latitude.toString(), locationModel.longitude.toString())
            stores.value = list
        }
    }
}