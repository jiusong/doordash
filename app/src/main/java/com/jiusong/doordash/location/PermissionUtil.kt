package com.jiusong.doordash.location

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

/**
 * Created by jiusong.gao on 1/30/21.
 */
object PermissionUtil {

    const val REQUEST_LOCATION_PERMISSION = 100

    fun hasLocationPermission(context: Context): Boolean {
        return hasPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    fun requestLocationPermission(activity: AppCompatActivity) {
        requestPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION, REQUEST_LOCATION_PERMISSION)
    }

    private fun hasPermission(context: Context, permission: String): Boolean {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            return false
        }
        return true
    }

    private fun requestPermission(activity: AppCompatActivity, permission: String, requestCode: Int) {
        ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)
    }
}