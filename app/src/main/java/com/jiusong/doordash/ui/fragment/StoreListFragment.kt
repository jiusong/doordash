package com.jiusong.doordash.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jiusong.doordash.data.network.Status
import com.jiusong.doordash.databinding.FragmentListBinding
import com.jiusong.doordash.ui.activity.RestaurantDetailActivity
import com.jiusong.doordash.ui.recycerview.StoreItemClickListener
import com.jiusong.doordash.ui.recycerview.StoreListAdapter
import com.jiusong.doordash.util.DoorDashConstants
import com.jiusong.doordash.viewmodel.StoresViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Created by jiusong.gao on 1/29/21.
 */
@AndroidEntryPoint
class StoreListFragment: Fragment(), StoreItemClickListener {

    private lateinit var binding: FragmentListBinding
    private val viewModel: StoresViewModel by viewModels()
    @Inject
    lateinit var storesAdapter: StoreListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListBinding.inflate(inflater)
        setupStoreList()
        return binding.root
    }

    private fun setupStoreList() {
        val viewManager = LinearLayoutManager(activity)
        storesAdapter.setListener(this)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = storesAdapter
        }
        observeStores()
        // Handle scroll to the end of the list
        handleListScrolling()
        // Fetch stores
        viewModel.loadStores()
    }

    private fun observeStores() {
        viewModel.stores.observe(viewLifecycleOwner, Observer {
            when(it.status) {
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(activity, it.msg, Toast.LENGTH_LONG)
                        .show()
                }
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    it.data?.let { list -> storesAdapter.addStores(list) }
                }
            }
        })
    }

    private fun handleListScrolling() {
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                // Fetch more stores.
                if (!recyclerView.canScrollVertically(1)) {
                    viewModel.loadMoreStores()
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })
    }

    override fun onClick(storeId: String) {
        val intent = Intent(activity, RestaurantDetailActivity::class.java)
        intent.putExtra(DoorDashConstants.STORE_ID, storeId)
        startActivity(intent)
    }

}