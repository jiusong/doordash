package com.jiusong.doordash.ui.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.jiusong.doordash.data.model.Store
import com.jiusong.doordash.data.network.Status
import com.jiusong.doordash.location.PermissionUtil
import com.jiusong.doordash.viewmodel.MapViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by jiusong.gao on 1/29/21.
 */
@AndroidEntryPoint
class StoreMapFragment: SupportMapFragment() {

    private val viewModel: MapViewModel by viewModels()


    override fun onActivityCreated(p0: Bundle?) {
        super.onActivityCreated(p0)
        viewModel.stores.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS) {
                markMapWithStores(it.data)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (PermissionUtil.hasLocationPermission(view.context)) {
            startLocationUpdate()
        } else {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), PermissionUtil.REQUEST_LOCATION_PERMISSION)
        }
    }

    private fun startLocationUpdate() {
        viewModel.locationData.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                viewModel.locationData.removeObservers(viewLifecycleOwner)
                viewModel.loadNearByStores(it)
            }
        })
    }


    private fun markMapWithStores(stores: List<Store>?) {
        if (stores.isNullOrEmpty()) return
        getMapAsync {map ->
            if (map != null) {
                addMarks(stores, map)
                if (viewModel.locationData.value != null) {
                    val latLng = LatLng(viewModel.locationData.value!!.latitude, viewModel.locationData.value!!.longitude)
                    zoomMap(latLng, map)
                }
            }
        }
    }

    private fun addMarks(stores: List<Store>, map: GoogleMap) {
        for (store in stores) {
            val latLng = LatLng(store.location.lat, store.location.lng)
            map.addMarker(MarkerOptions().position(latLng).title(store.name))
        }
    }

    private fun zoomMap(latLng: LatLng, map: GoogleMap) {
        val cameraPosition = CameraPosition.builder().target(latLng).zoom(10.0f).build()
        val cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
        map.moveCamera(cameraUpdate)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PermissionUtil.REQUEST_LOCATION_PERMISSION) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                startLocationUpdate()
            }
        }
    }
}